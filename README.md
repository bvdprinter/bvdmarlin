# bVdMarlin

the bVd printer Config files

## setup arduino-cli linux

get version 1.1.9 (bugfix) of [marlin](https://github.com/MarlinFirmware/Marlin/archive/bugfix-1.1.x.zip)

replace the followig files [Configuration.h, Configuration_adv.h, pins_MEGATRONICS_3]

install [arduino-cli](https://github.com/arduino/arduino-cli) on your system
> before running the command create the bin folder in the home dir
> check if it's in the $PATH otherwise restart the system and check again

```bash
export PATH=$PATH:/home/pi/bin
curl -fsSL https://raw.githubusercontent.com/arduino/arduino-cli/master/install.sh | BINDIR=~/bin sh

```

check version

```bash
arduino-cli version

```

setup project from user folder

```bash
arduino-cli sketch new Marlin
rm ~/Marlin/Marlin.ino
mkdir ~/tmp
wget https://github.com/MarlinFirmware/Marlin/archive/bugfix-1.1.x.zip -O ~/tmp/bugfix-1.1.x.zip
unzip ~/tmp/bugfix-1.1.x.zip -d ~/tmp/defaultMarlin/
mv -v ~/tmp/defaultMarlin/Marlin-bugfix-1.1.x/Marlin/ ~/Marlin/
git clone git@gitlab.com:bvdprinter/bvdmarlin.git ~/tmp/personalMarlin/
mv -v ~/tmp/personalMarlin/* ~/Marlin/
mv -v ~/tmp/personalMarlin/.* ~/Marlin/
rm -R ~/tmp
```

## install arduino core pakages

```bash
arduino-cli core update-index
arduino-cli board list
arduino-cli core install arduino:avr
arduino-cli lib search "TMC2130stepper"
arduino-cli lib install "TMC2130Stepper"
```

## compile arduino

```bash
arduino-cli compile --fqbn arduino:avr:mega Marlin
```

## upload new marlin code

```bash
arduino-cli upload -p /dev/ttyUSB0 --fqbn arduino:avr:mega Marlin
```
