
compile:
	echo "compile code"
	arduino-cli compile --fqbn arduino:avr:mega ~/Marlin
	echo "compile done"

upload:
	echo "upload to microcontroller"
	arduino-cli upload -p /dev/ttyUSB0 --fqbn arduino:avr:mega ~/Marlin
	echo "upload done"

update:
	echo "start"
	echo "pull latest changes"
	git pull
	make compile
	make upload
	echo "update complete"